=================================================
RN-Simplex: Regular N-dimensional Simplex Builder
=================================================

Brief Description
=================

Regular n-dimensional simplex builder with optional translation and scaling.



Installation
============

The ``RN-Simplex`` project is published on the `Python Package Index (PyPI) <https://pypi.org>`_ at: `https://pypi.org/project/rn-simplex <https://pypi.org/project/rn-simplex>`_. It requires version 3.8, or newer, of the interpreter. It should be installable from Python distribution platforms or Integrated Development Environments (IDEs). Otherwise, it can be installed from a command-line console:

- For all users, after acquiring administrative rights:
    - First installation: ``pip install rn-simplex``
    - Installation update: ``pip install --upgrade rn-simplex``
- For the current user (no administrative rights required):
    - First installation: ``pip install --user rn-simplex``
    - Installation update: ``pip install --user --upgrade rn-simplex``



Documentation
=============

The project provides a package ``rn_simplex`` with 2 modules: the main one, ``simplex``, and a basic test module, ``test_simplex``. The module ``simplex`` defines 2 functions: ``Simplex``, to build a regular n-dimensional simplex, and ``IsARegularSimplex``, to check whether a set of vertices defines a regular simplex.

The ``Simplex`` function takes a dimension (an integer) as a mandatory argument and the following optional, keyword-only arguments:

- ``centered``: A boolean to request the centering of the simplex. ``False`` by default.
- ``around``: A `Numpy array <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`_ to request the translation and scaling of the simplex so that its vertices lie on the smallest bounding ball of ``around`` (computed with `miniball <https://pypi.org/project/miniball>`_), or ``None``. The array shape must be ``(n_points, dimension_of_simplex)``. ``None`` by default.
- ``with_a_margin``: A float to request a positive, additive margin when passing ``around``, or ``None``. ``None`` by default.
- ``with_m_margin``: A float to request a multiplicative margin when passing ``around``, or ``None``. If strictly smaller than 1, then 1 is added internally. For example, 0.2 and 1.2 are both interpreted as a 20% margin. ``None`` by default.

The Python signature with type hints is:

.. code-block:: python

    Simplex(
        dimension: int,
        /,
        *,
        centered: bool = False,
        around: numpy.ndarray = None,
        with_a_margin: float = None,
        with_m_margin: float = None,
    ) -> numpy.ndarray

The ``IsARegularSimplex`` function takes a simplex `Numpy array <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`_ as a mandatory argument and the following optional, keyword-only arguments:

- ``tolerance``: A float specifying the maximum allowed difference between the minimum and maximum vertex pairwise distances. `Numpy float epsilon <https://numpy.org/doc/stable/reference/generated/numpy.finfo.html>`_ by default.
- ``return_issues``: A boolean to request the list of issues of the simplex as a sequence of ``str``.

The Python signature with type hints is:

.. code-block:: python

    IsARegularSimplex(
        check: numpy.ndarray,
        /,
        *,
        tolerance: float = nmpy.finfo(nmpy.float).eps,
        return_issues: bool = False,
    ) -> Union[bool, Tuple[bool, Sequence[str]]]

The basic test module can be run with:

``python -m rn_simplex.test_simplex``



Thanks
======

The project is developed with `PyCharm Community <https://www.jetbrains.com/pycharm>`_.

The development relies on several open-source packages (see ``install_requires`` in ``setup.py``).

The code is formatted by `Black <https://github.com/psf/black>`_, *The Uncompromising Code Formatter*.

The imports are ordered by `isort <https://github.com/timothycrosley/isort>`_... *your imports, so you don't have to*.
